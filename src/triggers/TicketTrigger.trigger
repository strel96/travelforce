trigger TicketTrigger on Ticket__c (after update) {
        TicketTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
}