import {LightningElement, api, wire, track} from 'lwc';
import {getRecord} from "lightning/uiRecordApi";
import {subscribe, unsubscribe} from 'lightning/empApi';
import {ShowToastEvent} from "lightning/platformShowToastEvent";
import {RefreshEvent} from "lightning/refresh";
import modal from 'c/modal';

//Apex
import getFlights from '@salesforce/apex/FlightController.getFlightData';
import saveFlight from '@salesforce/apex/FlightController.saveSelectedFlight';
import cancelFlight from '@salesforce/apex/FlightController.cancelFlight';
//Labels
import errorTtl from '@salesforce/label/c.Error_Ttl';
import successTtl from '@salesforce/label/c.Success_Ttl';
import bookBtn from '@salesforce/label/c.Book_Btn';
import removeBtn from '@salesforce/label/c.Remove_Btn';
import noRecordsMsg from '@salesforce/label/c.No_Records_Msg';
import flightCancelledMsg from '@salesforce/label/c.Flight_Canceled_Msg';
import flightBookedMsg from '@salesforce/label/c.Flight_Booked_Msg';
import bookSelectedFlightMsg from '@salesforce/label/c.Book_Selected_Flight_Ttl';
import removeSelectedFlightMsg from '@salesforce/label/c.Remove_Selected_Flight_Ttl';
import selectedFlightConfirmation from '@salesforce/label/c.Select_Flight_Confirmation';
import removeSelectedFlightConfirm from '@salesforce/label/c.Remove_Selected_Flight_Confirmation';
import selectedFlightTtl from '@salesforce/label/c.Selected_Flight_Ttl';
import availableFlightTtl from '@salesforce/label/c.Available_Flights_Ttl';
import ticketsLeftLbl from '@salesforce/label/c.Tickets_Left_Lbl';
import flightNumberLbl from '@salesforce/label/c.Flight_Number_Lbl';
import dateOfDepartureLbl from '@salesforce/label/c.Date_of_Departure_Lbl';
import timeOfDepartureLbl from '@salesforce/label/c.Time_of_Departure_Lbl';
//Schema
import FLIGHT_OBJECT from '@salesforce/schema/Flight__c';
import TRIP_PREFERRED_DAY from "@salesforce/schema/Trip__c.Preferred_Start_Date__c";
import TRIP_FLIGHT from "@salesforce/schema/Trip__c.Flight__c";
import TRIP_CONTACT from "@salesforce/schema/Trip__c.Contact__c";
//Platform Events
const EVENT_NAME = '/event/Ticket_Booked__e';
//Constants
const TRIP_FIELDS = [TRIP_PREFERRED_DAY, TRIP_FLIGHT, TRIP_CONTACT];
const PAGE_SIZE = 5; // Number of records to display per page

export default class FlightsComponent extends LightningElement {
    @api recordId;
    @api objectApiName;

    @track tripInfo = {};
    @track flightByIdsMap = [];
    @track isLoading = false;
    @track showRemoveModal = false;
    @track showBookModal = false;
    @track pageSize = PAGE_SIZE; // Number of records to display per page
    @track currentPage = 1;

    flightObjectName = FLIGHT_OBJECT;
    selectedFlight_ = {};
    forceRefresh = false;
    subscription = {};

    label = {
        errorTtl,
        successTtl,
        bookBtn,
        removeBtn,
        noRecordsMsg,
        flightCancelledMsg,
        flightBookedMsg,
        bookSelectedFlightMsg,
        removeSelectedFlightMsg,
        selectedFlightConfirmation,
        removeSelectedFlightConfirm,
        selectedFlightTtl,
        availableFlightTtl,
        ticketsLeftLbl,
        flightNumberLbl,
        dateOfDepartureLbl,
        timeOfDepartureLbl
    };

    set selectedFlight(value){
        this.selectedFlight_ = value;
    }

    get selectedFlight(){
        if(this.isEmpty(this.selectedFlight_)) return ;
        const startDate = new Date(this.selectedFlight_.Start_Date__c);
        this.selectedFlight_.dateOfDeparture = `${startDate.getFullYear()}-${startDate.getMonth() - 1}-${startDate.getDate()}`;
        this.selectedFlight_.timeOfDeparture = `${startDate.getHours()}:${startDate.getMinutes() < 10 ? '0' + startDate.getMinutes() : startDate.getMinutes()}`;

        return this.selectedFlight_;
    }

    @wire(getRecord, {recordId: "$recordId", fields: TRIP_FIELDS})
    wireTripAndFlightsData({error, data}) {
        if (data) {
            this.tripInfo = data;
            //to prevent from record form force refresh
            if (!this.forceRefresh) {
                this.getFlights(data);
            }
            this.forceRefresh = false;
        } else if (error) {
            this.fireToastEvent(error.message, this.label.errorTtl, 'error');
        }
    }

    getFlights(data){
        this.showSpinner();
        getFlights({
            startDate: data.fields.Preferred_Start_Date__c.value,
            flightId: data.fields.Flight__c.value
        })
            .then(result => {
                this.flightByIdsMap = result.availableFlights;
                this.selectedFlight = result.selectedFlight;
            })
            .catch(error => this.fireToastEvent(error.message, this.label.errorTtl, 'error'))
            .finally(() => this.hideSpinner());
    }

    get displayedRecords(){
        const start = (this.currentPage - 1) * this.pageSize;
        const end = start + this.pageSize;
        return this.flights()?.slice(start, end);
    }

    get disablePrevious(){
        return this.currentPage === 1;
    }

    get disableNext(){
        return this.currentPage === this.totalPages;
    }

    get totalPages(){
        return Math.ceil(this.flights().length / this.pageSize);
    }

    get fromToRecords(){
        const start = (this.currentPage - 1) * this.pageSize + 1;
        const end = start + this.pageSize - 1;
        return `Records  ${start} - ${end}`
    }

    get hidePagination(){
        return this.flights().length < this.pageSize;
    }

    handlePageChange(event) {
        const action = event.target.label;
        if (action === 'previous' && this.currentPage > 1) {
            this.currentPage -= 1;
        } else if (action === 'next' && this.currentPage < this.totalPages) {
            this.currentPage += 1;
        }
    }

     flights(){
        return this.flightByIdsMap ? Object.values(this.flightByIdsMap) : [];
    }
    get isFlightSelected(){
        return !this.isEmpty(this.selectedFlight_);
    }

    connectedCallback() {
        this.subscribeEvent();
    }

    subscribeEvent(){
        //Allows to refresh the page when someone has already booked a ticket
        const messageCallback = (response) => {
            const flightId = response.data.payload.Flight_Id__c;
            const isBooked = response.data.payload.Is_Booked__c;
            let flightByIdsMap = Object.assign({}, this.flightByIdsMap);
            let flight = flightByIdsMap[flightId];
            if (flight && isBooked){
                flight.Tickets__r.splice(0, 1);
            } else if(flight){
                flight.Tickets__r.push([]);
            }
            this.flightByIdsMap = flightByIdsMap;
        };

        subscribe(EVENT_NAME, -1, messageCallback).then(response => this.subscription = response);
    }

    showSpinner(){
        this.isLoading = true;
    }

    hideSpinner(){
        this.isLoading = false;
    }

    fireToastEvent(message, title, variant){
        const toast = new ShowToastEvent({
            title: title,
            variant: variant,
            message: message,
        });

        dispatchEvent(toast);
    }

    isEmpty(value) {
        if (value === undefined || value === null) {
            return true;
        }
        if (Array.isArray(value)) {
            return value.length === 0;
        } else if (typeof value === 'object') {
            return Object.keys(value).length === 0 && value.constructor === Object;
        } else if (value === 0) {
            return false;
        } else if (typeof value === 'string' || value instanceof String) {
            return !value.trim();
        } else {
            return !Boolean(value);
        }
    }

    handleBookButtonClick(event) {
        let flightId = event.currentTarget.dataset.id;
        if (this.isFlightSelected) {
            this.openConfirmation(
                this.label.bookSelectedFlightMsg,
                this.label.selectedFlightConfirmation
            )
                .then(confirm => {
                if (confirm) {
                    this.cancelFlight(this.selectedFlight_.Id);
                    this.saveFlight(flightId);
                }
            });
        } else {
            this.saveFlight(flightId);
        }
    }

    handleCancelFlightClick() {
        this.openConfirmation(
            this.label.removeSelectedFlightMsg,
            this.label.removeSelectedFlightConfirm
        )
            .then(confirm => {
            if (confirm) {
                this.cancelFlight(this.selectedFlight_.Id);
            }
        });
    }

    async openConfirmation(header, message) {
        return await modal.open({
            size: 'small',
            header: header,
            content: message
        });
    }

    saveFlight(flightId) {
        this.showSpinner();
        saveFlight({flightId: flightId, contactId: this.tripInfo.fields.Contact__c.value, tripId: this.recordId})
            .then((response) => {
                this.refreshRecordForm();
                this.selectedFlight = this.flightByIdsMap[flightId];
                this.fireToastEvent(
                    this.label.flightBookedMsg + ' ' + response,
                    this.label.successTtl,
                    'success'
                )
            })
            .catch(error => this.fireToastEvent(error.message, this.label.errorTtl, 'error'))
            .finally(() => this.hideSpinner());
    }

    cancelFlight(flightId) {
        this.showSpinner();
        cancelFlight({flightId: flightId, contactId: this.tripInfo.fields.Contact__c.value, tripId: this.recordId})
            .then(() => {
                this.refreshRecordForm();
                this.selectedFlight = null;
                this.toggleBookButton(flightId);
                this.fireToastEvent(this.label.flightCancelledMsg, this.label.successTtl, 'success')
            })
            .catch(error => this.fireToastEvent(error.message, this.label.errorTtl, 'error'))
            .finally(() => this.hideSpinner());
    }

    refreshRecordForm(){
        this.forceRefresh = true;
        this.dispatchEvent(new RefreshEvent());
    }

    toggleBookButton(flightId){
        const button = this.template.querySelector(`lightning-button[data-id=${flightId}]`);
        if (button){
            button.disabled = !!this.selectedFlight_;
        }
    }

    renderedCallback() {
        if(this.isFlightSelected){
            this.toggleBookButton(this.selectedFlight_.Id);
        }
    }

    disconnectedCallback() {
        unsubscribe(this.subscription);
    }
}