import { api } from 'lwc';
import LightningModal from 'lightning/modal';
//Labels
import cancelBtn from '@salesforce/label/c.Cancel_Btn';
import confirmBtn from '@salesforce/label/c.Confirm_Btn';


export default class Modal extends LightningModal {
    @api header;
    @api content;

    labels = {
        cancelBtn,
        confirmBtn
    };

    handleConfirm() {
        this.close('confirm');
    }
    handleCancel() {
        this.close();
    }
}