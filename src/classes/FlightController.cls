public with sharing class FlightController {
    private static final FlightService flightsService = new FlightService();
    @AuraEnabled
    public static FlightsWrapper getFlightData(String startDate, String flightId) {
        try {
            return new FlightsWrapper(
                    new Map<Id, Flight__c>(flightsService.getFlightsByStartDate(startDate)),
                    String.isEmpty(flightId) ? null : flightsService.getFlightById(flightId)
            );
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static String saveSelectedFlight(String flightId, String contactId, String tripId) {
        try {
           return flightsService.saveSelectedFlight(flightId, contactId, tripId);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    @AuraEnabled
    public static void cancelFlight(String flightId, String contactId, String tripId) {
        try {
            flightsService.cancelFlight(flightId, contactId, tripId);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    private class FlightsWrapper {
        @AuraEnabled
        public Map<Id,Flight__c> availableFlights;
        @AuraEnabled
        public Flight__c selectedFlight;

        public FlightsWrapper(Map<Id,Flight__c> availableFlights, Flight__c selectedFlight){
            this.availableFlights = availableFlights;
            this.selectedFlight = selectedFlight;
        }
    }
}