public with sharing class FlightSelector {
    public List<Flight__c> getFlightsByStartDate(String startDate) {
        return [SELECT
                Id,
                Name,
                Start_Date__c,
                (SELECT Id, Flight__c, Contact__c FROM Tickets__r WHERE Flight__c != NULL AND Contact__c = NULL)
        FROM Flight__c
        WHERE DAY_ONLY(Start_Date__c) = :Date.valueOf(startDate)
        AND Id IN (SELECT Flight__c FROM Ticket__c WHERE Flight__c != NULL AND Contact__c = NULL)
        ORDER BY Start_Date__c
        ];
    }

    public Flight__c getFlightWithAvailableTickets(String Id) {
        return [SELECT Id, Name, Start_Date__c, (SELECT Id, Flight__c, Contact__c FROM Tickets__r WHERE Contact__c = NULL) FROM Flight__c WHERE Id = :Id];
    }

    public Database.UpsertResult updateTicket(Id ticketId, String contactId) {
        return Database.upsert(new Ticket__c(Id = ticketId, Contact__c = contactId));
    }

    public void updateTrip(String tripId, String flightId) {
        Database.upsert(new Trip__c(Id = tripId, Flight__c = flightId, Status__c = 'booked'));
    }

    public Flight__c getFlightById(Id flightId) {
        return [SELECT Id, Name, Start_Date__c FROM Flight__c WHERE Id = :flightId];
    }

    public Flight__c getSelectedFlightWithTicket(Id flightId, Id contactId) {
        return [SELECT Id, Name, Start_Date__c, (SELECT Id, Flight__c, Contact__c FROM Tickets__r WHERE Contact__c = :contactId) FROM Flight__c WHERE Id = :flightId];
    }

}