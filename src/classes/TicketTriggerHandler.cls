public with sharing class TicketTriggerHandler {
    public static void onAfterUpdate(List<Ticket__c> newTickets, Map<Id, Ticket__c> oldTicketMap) {
        List<Ticket_Booked__e> ticketEvent = new List<Ticket_Booked__e>();
        for (Ticket__c ticket : newTickets) {
            if ((ticket.Flight__c != null && ticket.Contact__c != null) &&
                    (oldTicketMap.get(ticket.Id).Flight__c != null && oldTicketMap.get(ticket.Id).Contact__c == null)
            ) {
                ticketEvent.add(new Ticket_Booked__e(Flight_Id__c = ticket.Flight__c, Is_Booked__c = true));
            } else if ((ticket.Flight__c != null && ticket.Contact__c == null) &&
                    (oldTicketMap.get(ticket.Id).Flight__c != null && oldTicketMap.get(ticket.Id).Contact__c != null)
            ) {
                ticketEvent.add(new Ticket_Booked__e(Flight_Id__c = ticket.Flight__c, Is_Booked__c = false));
            }
        }
        EventBus.publish(ticketEvent);
    }
}