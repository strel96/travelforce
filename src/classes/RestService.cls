public with sharing class RestService {
    public static HttpResponse makeRequest(String endpoint, String method, String body, Map<String, String> headers){
        HttpRequest request = new HttpRequest();
        request.setBody(body);
        request.setEndpoint(endpoint);
        request.setMethod(method);
        if (!headers.isEmpty()) {
            for (String key : headers.keySet()) {
                String value = headers.get(key);
                request.setHeader(key, value);
            }
        }
        return new Http().send(request);
    }
}