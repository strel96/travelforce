public with sharing class FlightService {
    private final FlightSelector flightSelector = new FlightSelector();

    private static final String ENDPOINT = 'https://private-anon-52181a112d-testassign.apiary-mock.com/ticket';
    private static final String HTTP_POST = 'POST';
    private final Map<String, String> HEADERS = new Map<String, String>{
            'Content-Type' => 'application/json'
    };
    public List<Flight__c> getFlightsByStartDate(String startDate) {
        return flightSelector.getFlightsByStartDate(startDate);
    }

    public String saveSelectedFlight(String flightId, String contactId, String tripId) {
        Flight__c flight = flightSelector.getFlightWithAvailableTickets(flightId);
        Database.UpsertResult ticketResult;
        String responseMessage;
        // Double check that tickets are still available
        if (flight.Tickets__r.size() > 0) {
//            responseMessage = this.callTravelForce(flight.Tickets__r[0]);
            ticketResult = flightSelector.updateTicket(flight.Tickets__r[0].Id, contactId);
            if (ticketResult.isSuccess()) {
                flightSelector.updateTrip(tripId, flightId);
            } else {
                throw new ThrowFlightException(ticketResult.getErrors()[0].getMessage());
            }
        } else {
            throw new ThrowFlightException(System.Label.Tickets_Already_Booked_Msg);
        }
        return 'Success';
    }

    public void cancelFlight(String flightId, String contactId, String tripId) {
        Flight__c flight = flightSelector.getSelectedFlightWithTicket(flightId, contactId);
        Database.upsert(new List<SObject>{
                new Ticket__c(Id = flight.Tickets__r[0].Id, Contact__c = null),
                new Trip__c(Id = tripId, Flight__c = null, Status__c = 'search')
        });
    }

    public Flight__c getFlightById(Id flightId) {
        return flightSelector.getFlightById(flightId);
    }

    private String callTravelForce(Ticket__c ticket){
        HttpResponse response = RestService.makeRequest(ENDPOINT, HTTP_POST, JSON.serialize(ticket), HEADERS);
        if(response.getStatus() == 'Created'){
            return response.getBody();
        } else {
            throw new ThrowFlightException('Error: ' + response.getStatusCode() + ' ' + response.getStatus());
        }
    }

    private class ThrowFlightException extends Exception {}
}